/**
 * Copyright (c) 2018-2019 PoleStar Studio. All Rights Reserved. <br>
 * Use is subject to license terms.<br>
 * <p>
 * 在此处填写文件说明
 */
package com.itheima.annotation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 填写功能简述.
 * <p>填写详细说明.<br>
 * @version v1.0
 * @author lianzuozheng
 * @date 2019/4/8 -8:36 
 */
public class AnnotationAssembleTest {
    public static void main(String[] args){
        String xmlpath = "com/itheima/annotation/beans.xml";
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext(xmlpath);
        UserController userController = (UserController)applicationContext.getBean("userController");
        userController.save();
    }

}
