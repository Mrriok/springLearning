/**
 * Copyright (c) 2018-2019 PoleStar Studio. All Rights Reserved. <br>
 * Use is subject to license terms.<br>
 * <p>
 * 在此处填写文件说明
 */
package com.itheima.annotation;

import org.springframework.stereotype.Repository;
import section1.UserDaoImpl;

/**
 * 填写功能简述.
 * <p>填写详细说明.<br>
 * @version v1.0
 * @author lianzuozheng
 * @date 2019/4/8 -8:13 
 */
@Repository("userDao")
public class UserDaolmpl implements UserDao {
    public void save(){
        System.out.println("userdao...save...");
    }

}
