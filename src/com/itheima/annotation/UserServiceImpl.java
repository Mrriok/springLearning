/**
 * Copyright (c) 2018-2019 PoleStar Studio. All Rights Reserved. <br>
 * Use is subject to license terms.<br>
 * <p>
 * 在此处填写文件说明
 */
package com.itheima.annotation;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 填写功能简述.
 * <p>填写详细说明.<br>
 * @version v1.0
 * @author lianzuozheng
 * @date 2019/4/8 -8:16 
 */
@Service("Service")
public class UserServiceImpl implements UserService {
    @Resource(name="userDao")
    private UserDao userDao;
    public void save() {
        this.userDao.save();
        System.out.println("userservice...save...");
    }
}
