/**
 * Copyright (c) 2018-2019 PoleStar Studio. All Rights Reserved. <br>
 * Use is subject to license terms.<br>
 * <p>
 * 在此处填写文件说明
 */
package com.itheima.aspectj.annotation;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * 切面类.
 * <p>说明.<br>
 *     在此类中编写环绕通知
 *     实现前置检查权限和后置记录日志功能
 *
 * @version v1.0
 * @author
 * @date 2019-04-05 -22:37 
 */
@Aspect
@Component
public class MyAspect {

    /** 定义切入点表达式  */
    @Pointcut("execution(* com.itheima.aspectj.*.*(..))")
    public void myPointcut() {

    }

    /**
     * 环绕通知
     * @param proceedingJoinPoint JoinPoint子接口，可执行的目标对象方法
     * @return 目标对象
     * @throws Throwable 异常
     */
    @Around("myPointcut()")
    public Object myAround(ProceedingJoinPoint proceedingJoinPoint)throws Throwable {
        //环绕通知开始，模拟权限检查
        check_permssions();
        //执行目标对象的方法
        Object obj = proceedingJoinPoint.proceed();
        //环绕通知结束，模拟记录日志
        write_log();
        //返回目标对象
        return obj;
    }

    /**
     * 模拟权限检查
     */
    public void check_permssions() {
        System.out.println("权限检查");
    }

    /**
     * 模拟记录日志
     */
    public void write_log() {
        System.out.println("记录日志");
    }

}
