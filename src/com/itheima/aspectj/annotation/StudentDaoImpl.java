/**
 * Copyright (c) 2018-2019 PoleStar Studio. All Rights Reserved. <br>
 * Use is subject to license terms.<br>
 * <p>
 * 在此处填写文件说明
 */
package com.itheima.aspectj.annotation;

import org.springframework.stereotype.Component;

/**
 * UserDaoImpl类.
 * <p>说明.<br>
 *     实现UserDao接口，实现addUesr方法和deleteUser方法。
 * @version v1.0
 * @author lianzuozheng
 * @date 2019-03-24 -21:55 
 */
@Component("student")
public class StudentDaoImpl implements UserDao {

    @Override
    public void addUser() {
        //执行添加用户操作...
        System.out.println("添加学生用户");
    }

    @Override
    public void deleteUser() {
        //执行删除用户操作...
        System.out.println("删除学生用户");
    }
}
