/**
 * Copyright (c) 2018-2019 PoleStar Studio. All Rights Reserved. <br>
 * Use is subject to license terms.<br>
 * <p>
 * 在此处填写文件说明
 */
package com.itheima.aspectj.annotation;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 基于注解的AspectJ编程测试.
 * <p>说明.<br>
 *     1.使用@Component注解，注入StudentDaoImpl和MyAspect类到Spring容器
 *     2.使用@Aspect注解，标注MyAspect类为切面类
 *     3.使用@Pointcut(execution...)注解，标注切入点，并设定切入点表达式
 *     4.使用@Around注解，标注环绕通知方法
 *     5.
 * @version v1.0
 * @author lianzuozheng
 * @date 2019-03-21 -18:00 
 */
public class Test {

    public static void main(String[] args) {
        // 指定Spring配置文件路径
        String xmlPath= "com/itheima/aspectj.annotation/beans.xml";
        // 创建Spring容器
        ApplicationContext ac = new ClassPathXmlApplicationContext(xmlPath);
        // 从容器中获取bean
        UserDao student = (UserDao) ac.getBean("student");
        // 使用bean
        student.addUser();
        student.deleteUser();


    }

}
