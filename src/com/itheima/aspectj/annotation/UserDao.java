/**
 * Copyright (c) 2018-2019 PoleStar Studio. All Rights Reserved. <br>
 * Use is subject to license terms.<br>
 * <p>
 * 在此处填写文件说明
 */
package com.itheima.aspectj.annotation;

/**
 * 用户Dao接口.
 * <p>说明.<br>
 *     操作用户的行为，添加和删除用户方法。
 * @version v1.0
 * @author lianzuozheng
 * @date 2019-03-24 -21:52 
 */
public interface UserDao {
    public void addUser();
    public void deleteUser();
}
