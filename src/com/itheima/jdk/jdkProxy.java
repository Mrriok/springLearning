/**
 * Copyright (c) 2018-2019 PoleStar Studio. All Rights Reserved. <br>
 * Use is subject to license terms.<br>
 * <p>
 * 在此处填写文件说明
 */
package com.itheima.jdk;

import java.lang.reflect.Proxy;
import com.itheima.aspect.MyAspect;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 填写功能简述.
 * <p>填写详细说明.<br>
 * @version v1.0
 * @author lianzuozheng
 * @date 2019/4/15 -10:46 
 */
public class jdkProxy implements InvocationHandler {
    //声明目标类接口
    private UserDao userDao;
    //创建代理方法
    public Object createproxy(UserDao userDao){
        this.userDao = userDao;
        //1.类加载器
        ClassLoader classLoader = jdkProxy.class.getClassLoader();
        //2.被代理对象实现的所有接口
        Class[] clazz = userDao.getClass().getInterfaces();
        //3.使用代理类，进行增强，返回后的是代理后的对象
        return Proxy.newProxyInstance(classLoader,clazz,this);
    }
    /*
     1.所有动态代理类的方法调用，都会交由invoke()方法去处理
     2.proxy 被代理后的对象
     3.method 将要被执行的方法信息（反射）
     4.args 执行方法时需要的参数
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //声明切面
        MyAspect myAspect = new MyAspect();
        myAspect.check_permissions();
        Object obj = method.invoke(userDao,args);
        myAspect.log();
        return obj;
    }
}
