/**
 * Copyright (c) 2018-2019 PoleStar Studio. All Rights Reserved. <br>
 * Use is subject to license terms.<br>
 * <p>
 * 在此处填写文件说明
 */
package com.itheima.jdk;

/**
 * 填写功能简述.
 * <p>填写详细说明.<br>
 * @version v1.0
 * @author lianzuozheng
 * @date 2019/4/15 -11:18 
 */
public class jdkTest {
    public static void main(String[] args) {
        //创建代理对象
        jdkProxy jdkProxy = new jdkProxy();
        //创建目标对象
        UserDao userDao = new UserDaoImpl();
        //从代理对象中获取增强后的目标对象
        UserDao userDao1 = (UserDao) jdkProxy.createproxy(userDao);
        //执行方法
        userDao1.addUser();
        userDao1.deleteUser();
    }

}
