package com.jia.ioc;

public class StudentImpl implements Student{
	private String name;
	//声明Phone接口的引用变量phone，xml配置文件中已经配置好将要把bean Phone的PhoneImpl注入进来
	private Phone phone;
	//set注入：在StudentImpl中提供一个phone位置，以便Spring容器注入
	public void setPhone(Phone phone) {
		this.phone = phone;
	}
	public void learn() {
		//调用被注入的phone的call方法
		this.phone.call();
		System.out.println("I'm learning, please call me back letter.");
	}
	
}
