package com.jia.ioc;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestIoC {
	public static void main(String[] args) {
		//获取xml配置
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("com/jia/ioc/beans.xml");
		//java的多态，Student student（从抽象类student中实例） = ...（然后获取其实现类StudentImpl(getBeans是获取Student bean的StudentImpl)）
		Student student;
		student = (Student) applicationContext.getBean("Student");
		student.learn();
	}
}
