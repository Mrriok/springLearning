/**
 * Copyright (c) 2018-2019 PoleStar Studio. All Rights Reserved. <br>
 * Use is subject to license terms.<br>
 * <p>
 * 在此处填写文件说明
 */
package section1;

/**
 * 填写功能简述.
 * <p>填写详细说明.<br>
 * @version v1.0
 * @author lianzuozheng
 * @date 2019/3/25 -10:30 
 */
public class UserDaoImpl implements UserDao{
    public void say(){
        System.out.println("useDao,say Hello  World!");
    }

}
