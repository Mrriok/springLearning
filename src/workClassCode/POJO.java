/**
 * Copyright (c) 2018-2019 PoleStar Studio. All Rights Reserved. <br>
 * Use is subject to license terms.<br>
 * <p>
 * 在此处填写文件说明
 */
package workClassCode;

/**
 * pojo plain ordinary java object
 * 特点：1.所有属性都是私有的
 *       2.只有get和set是public
 *       3.没有继承extends
 *       4.没有接口implements实现
 *       注意：阿里的Java规约手册toString方法
 * @version v1.0
 * @author Mrrick_Chen
 * @date 2019/3/25 -8:15 
 */
public class POJO {


}
