/**
 * Copyright (c) 2018-2019 PoleStar Studio. All Rights Reserved. <br>
 * Use is subject to license terms.<br>
 * <p>
 * 在此处填写文件说明
 */
package workClassCode;

/**
 * javaBean是Java的基础组件，非新的技术，是一个规范
 * 1.私有属性
 * 2.public的get和set方法
 * 3.无参的结构
 * 4.序列化
 * javabean实列化方法
 * 1.使用构造器
 * 2.使用静态工厂的方式
 * 3.实列工厂的方式
 *
 * @version v1.0
 * @author lianzuozheng
 * @date 2019/3/25 -8:19 
 */
public class javaBeanfirst {

}
