/**
 * Copyright (c) 2018-2019 PoleStar Studio. All Rights Reserved. <br>
 * Use is subject to license terms.<br>
 * <p>
 * 在此处填写文件说明
 */
package workClassCode;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 填写功能简述.
 * <p>填写详细说明.<br>
 * @version v1.0
 * @author lianzuozheng
 * @date 2019/3/25 -9:22 
 */
public class test {
    public  static void main(String[] args){
        String xmlpath = "workClass/beans.xml";
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext(xmlpath);
        System.out.println(applicationContext.getBean("Bean"));
    }

}
